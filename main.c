#include <libssh2.h>
#include <stdio.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "config.h"

/* Variables */
int rc;
int local_port=22;
int want_port=0;
int sock, forwardsock=1;
LIBSSH2_SESSION *session;
LIBSSH2_CHANNEL *channel;
LIBSSH2_LISTENER *listener = NULL;

/* function headers */
int client_init();
int connect_server();
int create_session();
int listen_port();
int forward_port();
int close_server();
char* get_ip(char* hostbuffer);
int atoi(char* str);

int main(int argc, char * argv[]){
    if(argc > 1){
        local_port = atoi(argv[1]);
    }
    while(1){
        client_init();
        connect_server();
        create_session();
        listen_port();
        forward_port();
        close_server();
    }
}


int client_init(){
    rc = libssh2_init(0);
}

int connect_server(){
    struct sockaddr_in sin;
    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr(get_ip(server_ip));
    sin.sin_port = htons(server_port);
    connect(sock, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in));
}

char* get_key(char* path){
    struct passwd *pw = getpwuid(getuid());
    char *home = pw->pw_dir;
    char ret[strlen(home)+strlen(path)+8];
    strcpy(ret,home);
    strcat(ret,"/.ssh/");
    strcat(ret,path);
    return (char*)strdup(ret);
}

char* get_ip(char* hostbuffer){
    struct hostent *host_entry;
    host_entry = gethostbyname(hostbuffer);
    char *IPbuffer = inet_ntoa(*((struct in_addr*)host_entry->h_addr_list[0]));
    return IPbuffer;
}

int create_session(){

    session = libssh2_session_init();
    libssh2_trace(session,LIBSSH2_TRACE_AUTH);
    rc = libssh2_session_handshake(session, sock);
    rc = libssh2_userauth_publickey_fromfile_ex(session, username, strlen(username), get_key("id_rsa.pub"), get_key("id_rsa"), password);
    if(rc){
        rc = libssh2_userauth_password(session, username, password);
    }
}

int listen_port(){
    
    int remote_listenport;

    listener = libssh2_channel_forward_listen_ex(session, NULL, want_port, &remote_listenport, 1);
    if(want_port != remote_listenport){
        fprintf(stderr, "Server is listening on %d => %s:%d\n",
            local_port,
            server_ip,
            remote_listenport);
    }
    channel = libssh2_channel_forward_accept(listener);
    want_port = remote_listenport;
    libssh2_session_set_blocking(session, 0);
}

int forward_port(){
    int i;
    struct sockaddr_in sin;
    fd_set fds;
    struct timeval tv;
    ssize_t len, wr;
    char buf[16384];
    forwardsock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    sin.sin_family = AF_INET;
    sin.sin_port = htons(local_port);
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");

    connect(forwardsock, (struct sockaddr *)&sin, sizeof(struct sockaddr_in));

    libssh2_session_set_blocking(session, 0);

    while (1) {
        FD_ZERO(&fds);
        FD_SET(forwardsock, &fds);
        tv.tv_sec = 0;
        tv.tv_usec = 100000;
        rc = select(forwardsock + 1, &fds, NULL, NULL, &tv);
        while(rc && FD_ISSET(forwardsock, &fds)){
            len = recv(forwardsock, buf, sizeof(buf), 0);
            if(len <=0){
                break;
            }
            wr = 0;
            while (wr < len) {
                i = libssh2_channel_write(channel, buf, len);
                wr += i;
                // printf("%s",buf);
            }
        }
        while (1) {
            len = libssh2_channel_read(channel, buf, sizeof(buf));
            if(len == 0){
                return 0;
            }
            if (LIBSSH2_ERROR_EAGAIN == len)
                break;
            wr = 0;
            while (wr < len) {
                i = send(forwardsock, buf + wr, len - wr, 0);
                // printf("%s",buf);
                wr += i;
            }
        }
    }
    libssh2_channel_eof(channel);
}

int close_server(){
    fprintf(stderr, "Client disconnecting\n");
    if (channel)
        libssh2_channel_free(channel);
    if (listener)
        libssh2_channel_forward_cancel(listener);
    libssh2_session_disconnect(session, "Client disconnecting normally");
    libssh2_session_free(session);
    close(sock);
}
